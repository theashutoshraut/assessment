class Stack:
    def __init__(self):
        self.items=[]
    
    def is_empty(self):
         return self.items==[]
    
    def pushitem(self,data):
        self.items.append(data)
        print('\nAdded {} successfully!\n'.format(data))
        
    def popitem(self):
        return self.items.pop()
    
    def showitems(self):
        print("\nStack elements are : ")
        for i in self.items:
            print(i)


s=Stack()
while True:
    choice=int(input('\n1.push\n2.pop\n3.show stack\n4.quit\nEnter your choice:  '))
    
    if choice==1:
        pdata=input('Enter data to push:  ')
        s.pushitem(pdata)
        
        
    elif choice==2:
        if s.is_empty():
            print('\nStack is empty!\n')
        else:
            a=s.popitem()
            print('\nRemoved {} successfully!\n'.format(a))
            
            
    elif choice==3:
        if s.is_empty():
            print("\nStack is empty!\n")
        else:
            s.showitems()
            
            
    elif choice==4:
        break
        
        
    else:
        print('\nEnter valid option!\n')
        
        